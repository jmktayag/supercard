//
//  main.m
//  SuperCard
//
//  Created by Mckein on 12/5/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
