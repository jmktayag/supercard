//
//  PlayingCard.m
//  Matchismo
//
//  Created by Mckein on 11/28/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

@synthesize suit = _suit; //because we provide setter and getter

-(int)match:(NSArray *)otherCards{ //override designated initializer
    int score = 0;
    if ([otherCards count] == 1){
        id card = [otherCards firstObject]; //Instrospection
        if ([card isKindOfClass:[PlayingCard class]]){ //Introspection
            PlayingCard *otherCard = (PlayingCard *) card;
            if ([self.suit isEqualToString:otherCard.suit]){
                score = 1;
            }else if (self.rank == otherCard.rank){
                score = 4;
            }
        }
    }
    return score;
}

//Override parent Card method getter
-(NSString *)contents{
    NSArray *rankStrings = [PlayingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

//Class method -- you send it to a class not an instance(e.g. util methods etc.,)
+(NSArray *) validSuits{
    return @[@"♠︎",@"♣︎",@"♦︎",@"♥︎"];
}

+(NSArray *) rankStrings
{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+(NSUInteger) maxRank
{
    return [[self rankStrings] count] -1;
}

//If you implement the getter and the setter you are required do @synthesize
-(void)setSuit:(NSString *)suit
{
    if ([[PlayingCard validSuits] containsObject:suit]){
        _suit = suit;
    }
}

-(NSString *)suit{
    return _suit ? _suit : @"?";
}

//override setter of rank
-(void)setRank:(NSUInteger)rank{
    if (rank <= [PlayingCard maxRank]){
        _rank = rank;
    }
}



@end
