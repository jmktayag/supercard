//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Mckein on 11/28/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "Card.h"
@interface CardMatchingGame : NSObject

//this is my designated initializer
//if you subclass you must call this designated initializer
-(instancetype)initWithCardCount:(NSUInteger) count usingDeck:(Deck *)deck;

-(void)chooseCardAtIndex:(NSUInteger) index;
- (Card *) cardAtIndex:(NSUInteger) index;

@property (nonatomic, readonly) NSInteger score; //you set the access publicly readonly

@end
