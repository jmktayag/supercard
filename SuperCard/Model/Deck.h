//
//  Deck.h
//  Matchismo
//
//  Created by Mckein on 11/28/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

-(void) addCard:(Card *)card atTop:(BOOL)atTop;
-(void)addCard:(Card *) card;
-(Card *) drawRandomCard;

@end
