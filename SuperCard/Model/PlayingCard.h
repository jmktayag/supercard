//
//  PlayingCard.h
//  Matchismo
//
//  Created by Mckein on 11/28/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank; //64 bit unsigned int -- Iphone 5

+(NSArray *) validSuits;
+(NSUInteger) maxRank;


@end
