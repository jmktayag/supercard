//
//  PlayingCardDeck.m
//  Matchismo
//
//  Created by Mckein on 11/28/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@implementation PlayingCardDeck


//init always returns self
//instance type - return the same object instance

-(instancetype)init
{
    self = [super init]; //initialize super class
    
    if (self){
        for (NSString *suit in [PlayingCard validSuits]){
            for (NSUInteger rank = 1; rank <= [PlayingCard maxRank]; rank++){
                PlayingCard *card = [[PlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card];
            }
        }
    
    }
    
    return self;
    

}

@end
