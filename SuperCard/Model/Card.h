//
//  Card.h
//  Matchismo
//
//  Created by Mckein on 11/28/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
//all object live in the heap
//strong means keep strong pointer in the heap
//weak means if no one has strong pointer to it, it sets to nil
//non atomic - calling this setter and getter is not thread safe -- simple
//all properties start at nil/0
@property (strong,nonatomic) NSString *contents;

@property (nonatomic, getter = isChosen) BOOL chosen; //primitive type doesn't need strong or weak
@property (nonatomic, getter = isMatched) BOOL matched;

//Methods
-(int)match:(NSArray *) otherCards;

@end
