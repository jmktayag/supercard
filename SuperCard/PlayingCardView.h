//
//  PlayingCardView.h
//  SuperCard
//
//  Created by Mckein on 12/5/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayingCardView : UIView

@property (nonatomic) NSUInteger rank;
@property (nonatomic, strong) NSString *suit;
@property (nonatomic, getter = isFaceup) BOOL faceUp;

-(void) pinch: (UIPinchGestureRecognizer *) gesture;

@end
